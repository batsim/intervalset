{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=branch-off-24.11";
    flake-utils.url = "github:numtide/flake-utils";
    nur-kapack = {
      url = "github:oar-team/nur-kapack/master";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
  };

  outputs = { self, nixpkgs, nur-kapack, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        kapack = nur-kapack.packages.${system};
        release-options = {
          debug = false;
          doCoverage = false;
        };
        debug-cov-options = {
          debug = true;
          doCoverage = true;
        };
        debug-options = {
          debug = true;
          doCoverage = false;
        };
        base-defs = { cppMesonDevBase = nur-kapack.lib.${system}.cppMesonDevBase; };
        callPackage = mergedPkgs: deriv-func: attrset: options: pkgs.lib.callPackageWith(mergedPkgs // options) deriv-func attrset;
      in rec {
        functions = rec {
          intervalset = import ./nix/intervalset.nix;
          intervalset-test-binary = import ./nix/test-binary.nix;
          intervalset-test = import ./nix/test.nix;
          intervalset-coverage-report = import ./nix/coverage-report.nix;
          intervalset-sphinx-doc = import ./nix/sphinx-doc.nix;
          generate-packages = mergedPkgs: options: {
            intervalset = callPackage mergedPkgs intervalset {} options;
            intervalset-test-binary = callPackage mergedPkgs intervalset-test-binary {} options;
            intervalset-test = callPackage mergedPkgs intervalset-test {} options;
            intervalset-coverage-report = callPackage mergedPkgs intervalset-coverage-report {} options;
          };
        };
        packages-debug-cov = functions.generate-packages (pkgs // base-defs // packages-debug-cov) debug-cov-options;
        packages-debug = functions.generate-packages (pkgs // base-defs // packages-debug) debug-options;
        packages-release = functions.generate-packages (pkgs // base-defs // packages-release) release-options;
        packages = packages-release // {
          ci-intervalset-werror-gcc = callPackage pkgs functions.intervalset ({ stdenv = pkgs.gccStdenv; werror = true; } // base-defs) release-options;
          ci-intervalset-werror-clang = callPackage pkgs functions.intervalset ({ stdenv = pkgs.clangStdenv; werror = true; } // base-defs) release-options;
          ci-test = packages-debug-cov.intervalset-test;
          ci-coverage-report = packages-debug-cov.intervalset-coverage-report;
          sphinx-doc = callPackage pkgs functions.intervalset-sphinx-doc { sphinx_rtd_theme = pkgs.python3Packages.sphinx_rtd_theme; breathe = pkgs.python3Packages.breathe; } {};
        };
        devShells = {
          test = pkgs.mkShell rec {
            buildInputs = with packages-debug-cov; [
              intervalset
              intervalset-test-binary
              pkgs.cgdb pkgs.gdb
            ];
            DEBUG_SRC_DIRS = packages-debug-cov.intervalset-test-binary.DEBUG_SRC_DIRS;
            GDB_DIR_ARGS = packages-debug-cov.intervalset-test-binary.GDB_DIR_ARGS;
            shellHook = ''
              echo Found debug_info source paths. ${builtins.concatStringsSep ":" DEBUG_SRC_DIRS}
              echo Run the following command to automatically load these directories to gdb.
              echo gdb \$\{GDB_DIR_ARGS\}

              # create directory for gcov output files, and set gcov env vars accordingly
              mkdir -p /tmp/intervalset-test-gcda
              export GCOV_PREFIX=/tmp/intervalset-test-gcda
              export GCOV_PREFIX_STRIP=${packages-debug-cov.intervalset.GCOV_PREFIX_STRIP}
            '';
          };
        };
      }
    );
}


