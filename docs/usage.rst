Usage
=====

How to compile/link my program with intervalset?
------------------------------------------------
**intervalset** must be included and linked if one wants to use it.

- Ideally this is done via `pkg-config`_ after installing **intervalset** (cf. :ref:`install`). |br|
  ``pkg-config --cflags --libs intervalset``

Quick example
-------------
.. literalinclude:: ../test/usage_example.cpp
    :language: c++

.. _pkg-config: https://en.wikipedia.org/wiki/Pkg-config
.. |br| raw:: html

    <br />
