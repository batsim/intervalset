.. _install:

Install
=======

From Nix
--------

Nix_ is a package manager with amazing properties that is available on all Linux or Unix-like systems (*e.g.*, macOS).

**intervalset** is available on `NUR-Kapack`_ and directly from intervalset's git repository (flake or classical ``default.nix``).

Build It Yourself
-----------------

**intervalset** can be built with Meson_ and Ninja_.

.. code-block:: bash

    # Get the code
    git clone https://framagit.org/batsim/intervalset.git
    cd intervalset

    # Prepare build (call meson)
    meson build # install prefix can be changed with --prefix
    cd build

    # Build the library (and the unit tests if google-test is found)
    ninja

    # Install
    ninja install # may need privileges depending on prefix

.. note::
   | You first need to install **intervalset**'s dependencies. Notably:

   - decent C++ compiler
   - decent Meson
   - `Boost.Icl`_

   | Up-to-date dependencies are defined in the `intervalset Nix package`_.
   | Give a look at the `CI build output`_ for versions.

.. _Nix: https://nixos.org/nix/
.. _Meson: https://mesonbuild.com/
.. _Ninja: https://ninja-build.org/
.. _`Boost.Icl`: https://www.boost.org/doc/libs/release/libs/icl/doc/html/index.html
.. _`intervalset Nix package`: https://framagit.org/batsim/intervalset/blob/master/nix/intervalset.nix
.. _gtest: https://github.com/google/googletest
.. _`CI build output`: https://framagit.org/batsim/intervalset/-/jobs
.. _`NUR-Kapack`: https://github.com/oar-team/nur-kapack
