{ stdenv, lib
, doxygen
, sphinx, sphinx_rtd_theme, breathe
}:

stdenv.mkDerivation rec {
  name = "intervalset-doc";

  src = lib.sourceByRegex ../. [
    "^src"
    "^src/.*\.hpp"
    "^src/.*\.cpp"
    "^docs"
    "^docs/conf\.py"
    "^docs/.*\.rst"
    "^docs/Makefile"
    "^docs/Doxyfile"
    "^test"
    "^test/.*\.cpp"
  ];

  buildInputs = [
    doxygen
    sphinx
    sphinx_rtd_theme
    breathe
  ];

  buildPhase = "cd docs && make html";
  installPhase = ''
    mkdir -p $out
    cp -r _build/html $out/
  '';
}
