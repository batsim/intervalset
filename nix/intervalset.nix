{ stdenv, lib
, cppMesonDevBase
, meson, ninja, pkg-config
, boost
, debug ? false
, werror ? false
, doCoverage ? false
}:

(cppMesonDevBase {
  inherit stdenv lib meson ninja pkg-config debug werror doCoverage;
  coverageGcnoGlob = "libintervalset.so.p/*.gcno";
}).overrideAttrs(attrs: rec {
  name = "intervalset";
  src = lib.sourceByRegex ../. [
    "^meson\.build"
    "^src"
    "^src/.*\.hpp"
    "^src/.*\.cpp"
  ];
  propagatedBuildInputs = [ boost ];
  passthru = rec {
    GCOV_PREFIX_STRIP = "5";
    DEBUG_SRC_DIRS = [ "${src}/src" ];
    GDB_DIR_ARGS = map (x: "--directory=" + x) DEBUG_SRC_DIRS;
  };
})
