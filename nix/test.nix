{ stdenv, lib
, intervalset, intervalset-test-binary
, doCoverage ? false
}:

stdenv.mkDerivation rec {
  name = "intervalset-test";

  buildInputs = [
    intervalset-test-binary
  ];

  unpackPhase = "true"; # no src for this package
  buildPhase = lib.optionalString doCoverage ''
    mkdir -p gcda
    export GCOV_PREFIX=$(realpath gcda)
    export GCOV_PREFIX_STRIP=${intervalset.GCOV_PREFIX_STRIP}
  '' + ''
    intervalset-tests
  '';
  installPhase = ''
    mkdir -p $out
  '' + lib.optionalString doCoverage ''
    mv gcda $out/
  '';
}
