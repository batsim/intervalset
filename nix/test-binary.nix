{ stdenv, lib
, cppMesonDevBase
, gtest, intervalset
, meson, ninja, pkg-config
, debug ? false
, werror ? false
}:

(cppMesonDevBase {
  inherit stdenv lib meson ninja pkg-config debug werror;
  doCoverage = false;
}).overrideAttrs(attrs: rec {
  name = "intervalset-test-binary";
  src = lib.sourceByRegex ../test [
    "^meson\.build"
    "^.*?pp"
  ];
  buildInputs = [
    gtest
    intervalset
  ];
  passthru = rec {
    DEBUG_SRC_DIRS = intervalset.DEBUG_SRC_DIRS ++ [ "${src}" ];
    GDB_DIR_ARGS = map (x: "--directory=" + x) DEBUG_SRC_DIRS;
  };
})
