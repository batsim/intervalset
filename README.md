[![pipeline status](https://framagit.org/batsim/intervalset/badges/master/pipeline.svg)](https://framagit.org/batsim/intervalset/pipelines)
[![coverage report](https://framagit.org/batsim/intervalset/badges/master/coverage.svg)](https://framagit.org/batsim/intervalset/badges/master/coverage.svg)
[![doc status](https://img.shields.io/readthedocs/intervalset.svg)](http://intervalset.rtfd.io)

**intervalset** is a C++ library to manage sets of closed intervals of integers.
This is a simple wrapper around [Boost.Icl][Boost.Icl].

Installation and usage guides are available in [intervalset's documentation][readthedocs].  
Project history is kept in [intervalset's changelog][changelog].

[Boost.Icl]: https://www.boost.org/doc/libs/release/libs/icl/doc/html/index.html
[readthedocs]: https://intervalset.readthedocs.io/en/latest/
[changelog]: ./CHANGELOG.md
